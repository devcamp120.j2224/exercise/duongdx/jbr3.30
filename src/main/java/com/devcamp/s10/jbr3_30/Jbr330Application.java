package com.devcamp.s10.jbr3_30;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Jbr330Application {

	public static void main(String[] args) {
		SpringApplication.run(Jbr330Application.class, args);
	}

}
