package com.devcamp.s10.jbr3_30;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeRestAPI {
    @CrossOrigin
    @GetMapping("/employees")
    public ArrayList<Employee> Employee(){
        ArrayList<Employee> arrayList1 = new ArrayList<>();
            Employee employee1 = new Employee(2, "Dao", "Duong", 1000);
            Employee employee2 = new Employee(3, "Duy", "Tuan", 2000);
            Employee employee3 = new Employee(4, "Vu", "Toan", 3000);

        arrayList1.add(employee1);
        arrayList1.add(employee2);
        arrayList1.add(employee3);

        return arrayList1 ;
    }    
}
